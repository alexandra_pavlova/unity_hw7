using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private AudioSource bgMusic;
    private AudioSource clickSound;
    private AudioSource raidSound;
    private AudioSource villagerSound;
    private AudioSource defenderSound;
    
    public TimerHarvestScript harvestTimer;
    public TimerConsumptionScript consumptionTimer;

    public Image raidTimerImg;
    public Image villagerTimerImg;
    public Image defenderTimerImg;
    
    public Button villagerButton;
    public Button defenderButton;
    
    public Text resourcesText;
    public Text gameOverResultText;
    public Text nextRaidText;
    public Text winTextResult;
    
    public int villagerCount;
    public int defenderCount;
    public int wheatCount;
    
    public int villagerCost;
    public int defenderCost;

    public int wheatPerVillager;
    public int wheatForDefender;
    public int winWheat;
    public int villagerWinNumber;

    public float villagerHireTime;
    public float defenderHireTime;
    public float raidMaxTime;
    public int raidIncrease;
    public int nextRaid;
    public int raidDelay = 3;
    
    public GameObject GameOverScreen;
    public GameObject WinScreen;
    public GameObject PauseScreen;
    public GameObject RaidSoundHolder;
    public GameObject ClickSoundHolder;

    private float villagerTimer = -2;
    private float defenderTimer = -2;
    private float raidTimer;
    private int raidCount = -2;

    
    void Start()
    {
        UpdateText();
        bgMusic = GetComponent<AudioSource>();
        raidSound = RaidSoundHolder.GetComponent<AudioSource>();
        clickSound = ClickSoundHolder.GetComponent<AudioSource>();
        villagerSound = villagerButton.GetComponent<AudioSource>();
        defenderSound = defenderButton.GetComponent<AudioSource>();
        
        raidTimer = raidMaxTime;
    }
    
    void Update()
    {
        CheckDefenders();
        CheckVillagers();
        
        Raid();

        UpdateResources();
        UpdateText();

        CheckForWin();
        CheckForLose();
    }

    public void HireVillager()
    {
        clickSound.Play();
        wheatCount -= villagerCost;
        villagerTimer = villagerHireTime;
        villagerButton.interactable = false;
    }

    public void HireDefender()
    {
        clickSound.Play();
        wheatCount -= defenderCost;
        defenderTimer = defenderHireTime;
        defenderButton.interactable = false;
    }

    private void UpdateText()
    {
        resourcesText.text = villagerCount + "\n" + defenderCount + "\n\n" + wheatCount;
    }

    public void PauseTheGame()
    {
        clickSound.Play();
        if (Time.timeScale != 0)
        {
            Time.timeScale = 0; //остановить время
            PauseScreen.SetActive(true);
        }
        else
        {
            Time.timeScale = 1; //остановить время
            PauseScreen.SetActive(false);
        }
    }

    public void SoundOff()
    {
        clickSound.Play();
        if (bgMusic.isPlaying)
        {
            bgMusic.Pause();
            harvestTimer.GetComponent<AudioSource>().mute = true;
            consumptionTimer.GetComponent<AudioSource>().mute = true;
            clickSound.mute = true;
            villagerSound.mute = true;
            defenderSound.mute = true;
            raidSound.mute = true;
        }
        else
        {
            bgMusic.UnPause();
            harvestTimer.GetComponent<AudioSource>().mute = false;
            consumptionTimer.GetComponent<AudioSource>().mute = false;
            clickSound.mute = false;
            villagerSound.mute = false;
            defenderSound.mute = false;
            raidSound.mute = false;
        }
        
    }

    private void CheckDefenders()
    {
        if (wheatCount < defenderCost)
        {
            defenderButton.interactable = false;
            if (defenderTimer > 0)
            {
                defenderTimer -= Time.deltaTime;
                defenderTimerImg.fillAmount = defenderTimer / defenderHireTime;
            }
        }
        else if (defenderTimer > 0)
        {
            defenderTimer -= Time.deltaTime;
            defenderTimerImg.fillAmount = defenderTimer / defenderHireTime;
        }
        else if (defenderTimer > -1)
        {
            defenderSound.Play();
            defenderTimerImg.fillAmount = 1;
            defenderButton.interactable = true;
            defenderCount += 1;
            defenderTimer = -2;
        }
    }

    private void CheckVillagers()
    {
        if (wheatCount < villagerCost)
        {
            villagerButton.interactable = false;
            if (villagerTimer > 0)
            {
                villagerTimer -= Time.deltaTime;
                villagerTimerImg.fillAmount = villagerTimer / villagerHireTime;
            }
        }
        else if (villagerTimer > 0)
        {
            villagerTimer -= Time.deltaTime;
            villagerTimerImg.fillAmount = villagerTimer / villagerHireTime;
        }
        else if (villagerTimer > -1)
        {
            villagerSound.Play();
            villagerTimerImg.fillAmount = 1; //заполнение часов отсчета кнопки
            villagerButton.interactable = true;
            villagerCount += 1;
            villagerTimer = -2;
        }
    }

    private void CheckForWin()
    {
        if (wheatCount >= winWheat)
        {
            Time.timeScale = 0; //остановить время
            WinScreen.SetActive(true);
            winTextResult.text = "Your village started trade with a big town!";
        }
        else if (villagerCount >= villagerWinNumber)
        {
            Time.timeScale = 0; //остановить время
            WinScreen.SetActive(true);
            winTextResult.text = "Your village was given a 'Town' status!";
        }
    }

    private void CheckForLose()
    {
        if (wheatCount < 0)
        {
            Time.timeScale = 0; //остановить время
            GameOverScreen.SetActive(true);
            gameOverResultText.text = "Village is abandoned because of hunger..." + "\n" + "Villagers left: " + villagerCount;
        }
        if (defenderCount < 0)
        {
            Time.timeScale = 0; //остановить время
            GameOverScreen.SetActive(true);
            gameOverResultText.text = "Raids reflected: " + raidCount + "\n" + "Wheat amount lost: " + wheatCount +
                                      "\n" + "Villagers became slaves: " + villagerCount;
        }
    }

    private void UpdateResources()
    {
        if (harvestTimer.Tick)
        {
            wheatCount += villagerCount * wheatPerVillager;
        }
        if (consumptionTimer.Tick)
        {
            wheatCount -= defenderCount * wheatForDefender;
        }
    }

    private void Raid()
    {
        nextRaidText.text = "Cicles before Raid: " + raidDelay + "\n" + "Next Raid: " + nextRaid;
        raidTimer -= Time.deltaTime;
        raidTimerImg.fillAmount = raidTimer / raidMaxTime;
        
        if (raidTimer <= 0)
        {
            raidSound.Play();
            if (raidDelay >= 1)
            {
                raidDelay -= 1;
            }
            raidTimer = raidMaxTime;
            defenderCount -= nextRaid;
            if (raidDelay == 0)
            {
                nextRaid += raidIncrease;
                raidCount += 1;
            }
            nextRaidText.text = "Next Raid: " + nextRaid;
            
        }
    }
}
