using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerConsumptionScript : MonoBehaviour
{
    private Image img;
    public float MaxTime;
    public bool Tick;
    private float currentTime;
    private AudioSource eatSound;
    
    void Start()
    {
        eatSound = GetComponent<AudioSource>();
        img = GetComponent<Image>();
        currentTime = MaxTime;
    }

    void Update()
    {
        Tick = false;
        currentTime -= Time.deltaTime;
        if (currentTime <= 0)
        {
            Tick = true;
            currentTime = MaxTime;
            eatSound.Play();
        }
        img.fillAmount = 1 - (currentTime / MaxTime);
    }
}
